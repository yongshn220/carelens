﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit;

public class EyeGazeProviderTest : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        gameObject.transform.position = CoreServices.InputSystem.EyeGazeProvider.HitPosition;

        gameObject.transform.position =
        CoreServices.InputSystem.EyeGazeProvider.GazeOrigin +
        CoreServices.InputSystem.EyeGazeProvider.GazeDirection.normalized * 2;
    }
}
