﻿using System.Collections;
using System.Collections.Generic;
using Microsoft.MixedReality.Toolkit.UI;
using UnityEngine;

public class PersonalCareControl : MonoBehaviour
{
    public GameObject vitalPanel;
    public GameObject informationPanel;
    public GameObject schedulePanel;

    void Start()
    {
        StartSetting();
    }

    void StartSetting()
    {
        this.vitalPanel.SetActive(false);
        this.informationPanel.SetActive(false);
        this.schedulePanel.SetActive(false);
    }

#region BUTTON EVENT
    public void OnVitalButtonClick()
    {
        CloseAll();
        this.vitalPanel.SetActive(true);
    }

    public void OnInformationButtonClick()
    {
        CloseAll();
        this.informationPanel.SetActive(true);
    }

    public void OnScheduleButtonClick()
    {
        CloseAll();
        this.schedulePanel.SetActive(true);
    }

    public void OnExitButtonClick()
    {
        CloseAll();
        GameManager.instance.LoadMainScene();
    }

    private void CloseAll()
    {
        this.vitalPanel.SetActive(false);
        this.informationPanel.SetActive(false);
        this.schedulePanel.SetActive(false); 
    }
#endregion

}
