﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuListControl : MonoBehaviour
{   
    public GameObject mainMenu;
    public GameObject pageMenu;


    public void DefaultSetting()
    {
        ActivatePageMenu(false);
    }

    public void ActivatePageMenu(bool state)
    {
        this.pageMenu.SetActive(state);
    }
}
