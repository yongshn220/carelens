﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

public class ToggleControl : MonoBehaviour
{
    public void ActivateToggle(bool state)
    {
        gameObject.GetComponent<Interactable>().IsToggled = state;
    }
}
