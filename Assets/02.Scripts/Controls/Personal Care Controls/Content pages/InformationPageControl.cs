﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class InformationPageControl : MonoBehaviour
{
    public TMP_Text patientName;
    public TMP_Text birth;
    public TMP_Text age;
    public TMP_Text gender;
    public TMP_Text address;
    public TMP_Text nameOfDisease;
    public TMP_Text ward;
    public TMP_Text roomNumber;
    public TMP_Text Phone;

    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.patientUpdateEvent.AddListener(DataUpdate);
    }

    void DataUpdate(Patient patient)
    {
        this.patientName.text = patient.patient_info.name;
        this.birth.text = patient.patient_info.birth;
        this.age.text = patient.patient_info.age;
        this.gender.text = patient.patient_info.gender;
        this.address.text = patient.patient_info.address;
        this.nameOfDisease.text = patient.patient_info.disease_name;
        this.ward.text = patient.patient_info.ward;
        this.roomNumber.text = patient.patient_info.room_number;
        this.Phone.text = patient.patient_info.phone_number;
    }
}
