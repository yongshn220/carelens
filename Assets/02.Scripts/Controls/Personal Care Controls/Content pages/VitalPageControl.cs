﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;


public class VitalPageControl : MonoBehaviour
{
    public TMP_Text pulseRateText;
    public TMP_Text bloodPressureText;
    public TMP_Text respirationRateText;
    public TMP_Text bodyTemperatureText;


    // Start is called before the first frame update
    void Start()
    {
        GameManager.instance.patientUpdateEvent.AddListener(DataUpdate);
    }

    public void DataUpdate(Patient patient)
    {
        pulseRateText.text = patient.patient_vital.pulse_rate;
        bloodPressureText.text = patient.patient_vital.blood_pressure_high;
        respirationRateText.text = patient.patient_vital.respiration_rate;
        bodyTemperatureText.text = patient.patient_vital.body_temperature;
    }
}
