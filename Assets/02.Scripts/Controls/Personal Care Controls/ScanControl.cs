﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

public class ScanControl : MonoBehaviour
{

    public GameObject searchingTextObj;

    public void ActivateSearchingText(bool state)
    {
        searchingTextObj.SetActive(state);
    }

    public void OnButtonClicked(Interactable toggle)
    {
        ActivateSearchingText(toggle.IsToggled);
    }
}
