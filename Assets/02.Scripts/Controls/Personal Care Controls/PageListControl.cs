﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Utilities;
using Microsoft.MixedReality.Toolkit.UI;

public class PageListControl : MonoBehaviour
{
    public GameObject vitalPage;
    public GameObject informationPage;
    public GameObject schedulePage;


    // Start is called before the first frame update
    public void DefaultSetting()
    {
        this.vitalPage.SetActive(false);
        this.informationPage.SetActive(false);
        this.schedulePage.SetActive(false);
    }

    public void ActivatePages(bool state)
    {
        this.vitalPage.SetActive(state);
        this.informationPage.SetActive(state);
        this.schedulePage.SetActive(state);
    }

    public void OnVitalToggleClick(Interactable toggle)
    {
        bool state = toggle.IsToggled;
        
        this.vitalPage.SetActive(state);

        if(state)
        {
            this.UpdatePageList(); 
        }
    }

    public void OnInformationToggleClick(Interactable toggle)
    {
        bool state = toggle.IsToggled;

        this.informationPage.SetActive(state);

        if(state)
        {
            this.UpdatePageList(); 
        }
    }

    public void OnScheduleToggleClick(Interactable toggle)
    {
        bool state = toggle.IsToggled;

        this.schedulePage.SetActive(state);

        if(state)
        {
            this.UpdatePageList(); 
        }
    }

    public void UpdatePageList()
    {
        GetComponent<GridObjectCollection>().UpdateCollection();
    }
}
