﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
/*
// Firebase 네임스페이스 선언
using Firebase;
using Firebase.Database;

*/

public class FBMgr : MonoBehaviour
{
    /*
    public string tagVal;

    private DatabaseReference rootReference;
    private DatabaseReference referencePatient;
    private DatabaseReference reference;
    private readonly string uri = "https://carelens-989e0-default-rtdb.firebaseio.com/";

    void Awake()
    {
        // 앱 속성 생성
        AppOptions options = new AppOptions();
        options.DatabaseUrl = new System.Uri(uri);

        // 앱을 생성
        FirebaseApp app = FirebaseApp.Create(options);

        // 루트 레퍼런스
        rootReference = FirebaseDatabase.DefaultInstance.RootReference;

        // reference = FirebaseDatabase.DefaultInstance.GetReference("temp");
        referencePatient = FirebaseDatabase.DefaultInstance.GetReference("patient");
    }

    void Start()
    {
        // 해당 병동의 전체 환자 정보 가져오기
        // GetAllPatients();

        // 코드로 환자 검색하기
        // SelectPatient("0000");
    }

    // 전체 환자 가져오기
    public void GetAllPatients()
    {
        Debug.Log("---------------GetAllPatients()-----------------");
        FirebaseDatabase.DefaultInstance.GetReference("patient")
                .GetValueAsync()
                .ContinueWith(task =>
                {
                    if (task.IsFaulted)
                    {
                        Debug.Log("1-1. fail");
                    }
                    else if (task.IsCompleted)
                    {
                        DataSnapshot snapshot = task.Result;
                        // temp(snapshot);



                        // data : 환자코드까지 접근 (0000, 0001 ...)
                        foreach (DataSnapshot data in snapshot.Children)
                        {
                            IDictionary temp = (IDictionary)data.Value;
                            Debug.Log($"코드 : {temp["code"]}");


                            // info : 환자의 personal_info에 접근
                            DataSnapshot info = data.Child("personal_info");
                            IDictionary infoData = (IDictionary)info.Value;
                            Debug.Log("이름: " + infoData["name"] + ", 나이: " + infoData["age"] + ", 주소: " + infoData["adress"]);


                            // state : 환자의 state 접근
                            DataSnapshot state = data.Child("state");
                            IDictionary stateData = (IDictionary)state.Value;
                            Debug.Log($"심박수: {stateData["heartRate"]}, 혈압 : {stateData["bloodPressure"]}, 체온 : {stateData["bodyHeat"]}, 호흡 : {stateData["respirationRate"]}");


                            Debug.Log($"------------------------------------");
                        }

                    }
                });
    }


    public Query userNameQuery;
    // public DataSnapshot snapshotPatient;

    // 키 값으로 검색
    public void SelectPatient(string code)
    {
        //! 테스트 값
        code = "0000";
        // 정렬, 추출 Query
        userNameQuery = referencePatient.OrderByChild("code").EqualTo(code);
        // 검색
        userNameQuery.ValueChanged += OnDataLoaded;
    }

    // Query 결과값을 출력하는 함수
    void OnDataLoaded(object sender, ValueChangedEventArgs args)
    {
        // 데이터 조회 스냅샷 정의
        DataSnapshot snapshot = args.Snapshot;

        // 해당 코드를 가진 환자가 없으면
        if (snapshot.ChildrenCount == 0)
        {
            Debug.Log("해당 코드를 가진 환자가 없음");
        }
        else
        {
            // data : 환자코드까지 접근  (0000, 0001 ...)
            foreach (var data in snapshot.Children)
            {
                IDictionary basicData = (IDictionary)data.Value;

                IDictionary infoData = (IDictionary)data.Child("personal_info").Value;
                Debug.Log("이름: " + infoData["name"] + ", 나이: " + infoData["age"] + ", 주소: " + infoData["adress"]);

                IDictionary stateData = (IDictionary)data.Child("state").Value;
                Debug.Log($"심박수: {stateData["heartRate"]}, 혈압 : {stateData["bloodPressure"]}, 체온 : {stateData["bodyHeat"]}, 호흡 : {stateData["respirationRate"]}");
                Debug.Log("-------------------------------------------");

                GameManager.instance.patient.SetInfo(basicData, infoData, stateData);
            }
        }
    }

    public Dictionary<string, string> ToDictionary(string key, string value)
    {
        Dictionary<string, string> result = new Dictionary<string, string>();
        result[key] = value;

        return result;
    }


    // GM\patient의 state 값을 변경하는 함수
    public void WriteStateData(string key, string value)
    {
        reference = FirebaseDatabase.DefaultInstance.GetReference("patient").Child(GameManager.instance.patient.code);
        reference.Child("state").Child(key).SetValueAsync(value);
    }

    public void WriteStateDataTest(string value)
    {
        //! 테스트용
        string key = "respirationRate";

        reference = FirebaseDatabase.DefaultInstance.GetReference("patient").Child(GameManager.instance.patient.code);
        reference.Child("state").Child(key).SetValueAsync(value);

    }
*/
}

