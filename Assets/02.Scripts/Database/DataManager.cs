using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using MRTK.Tutorials.AzureCloudServices.Scripts.Domain;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

// using Microsoft.Uii.Desktop.Core

namespace MRTK.Tutorials.AzureCloudServices.Scripts.Managers
{
    public class DataManager : MonoBehaviour
    {
        private string connectionString = "DefaultEndpointsProtocol=https;AccountName=carelens-db;AccountKey=8QurGPH2shU1N8rVPxKlBK9MQyO9M9HlaGoLtOoKIMCwWgYazjFLwIlXN9JwA2nQRwsEqiFe0gnyhtaZ7jkqTw==;TableEndpoint=https://carelens-db.table.cosmos.azure.com:443/;";

        private CloudStorageAccount storageAccount;
        private CloudTableClient cloudTableClient;
        private CloudTable table_info;
        private CloudTable table_vital;
        private CloudTable table_schedule;

        private void Awake()
        {
            storageAccount = CloudStorageAccount.Parse(connectionString);
            cloudTableClient = storageAccount.CreateCloudTableClient();
            table_info = cloudTableClient.GetTableReference("patient_info");
            table_vital = cloudTableClient.GetTableReference("patient_vital");
            table_schedule = cloudTableClient.GetTableReference("patient_schedule");
        }

        // Load a Selected Patient Data
        public async void LoadPatientData(string partitionKey, string rowKey)
        {
            TableOperation retrieveOperation;
            TableResult result;

            retrieveOperation = TableOperation.Retrieve<Patient_info>(partitionKey, rowKey);
            result = await table_info.ExecuteAsync(retrieveOperation);
            Patient_info info = result.Result as Patient_info;

            retrieveOperation = TableOperation.Retrieve<Patient_vital>(partitionKey, rowKey);
            result = await table_vital.ExecuteAsync(retrieveOperation);
            Patient_vital vital = result.Result as Patient_vital;

            retrieveOperation = TableOperation.Retrieve<Patient_schedule>(partitionKey, rowKey);
            result = await table_schedule.ExecuteAsync(retrieveOperation);
            Patient_schedule schedule = result.Result as Patient_schedule;

            // Create Patient Instance
            Patient newPatient = new Patient(partitionKey, rowKey, info, vital, schedule);

            // Check if this patient is in an emergency or not.
            if (partitionKey == GameManager.instance.emergencyPatientKey)
            {
                GameManager.instance.UpdateEmergencyPatientData(newPatient);
            }
            else
            {
                GameManager.instance.UpdatePatientData(newPatient);
            }
        }

        // Load a Selected Patent Vital Data
        public async void LoadPatientVitalData()
        {
            TableOperation retrieveOperation;
            TableResult result;

            Patient newPatient = GameManager.instance.patient;

            retrieveOperation = TableOperation.Retrieve<Patient_vital>(newPatient.pKey, newPatient.rKey);
            result = await table_vital.ExecuteAsync(retrieveOperation);
            Patient_vital vital = result.Result as Patient_vital;

            newPatient.patient_vital = vital;
            GameManager.instance.UpdatePatientData(newPatient);
        }

        // Load a Emergency Patent Vital Data
        public async void LoadEmergencyPatientVitalData()
        {
            TableOperation retrieveOperation;
            TableResult result;

            Patient newPatient = GameManager.instance.emergencyPatient;

            retrieveOperation = TableOperation.Retrieve<Patient_vital>(newPatient.pKey, newPatient.rKey);
            result = await table_vital.ExecuteAsync(retrieveOperation);
            Patient_vital vital = result.Result as Patient_vital;

            newPatient.patient_vital = vital;
            GameManager.instance.UpdateEmergencyPatientData(newPatient);
        }

        // Load All Patient Data
        public async void LoadAllPatientData()
        {
            PatientList newPatientList = new PatientList();

            // CloudTable table = this.projectsTable;
            var infoList = new List<Patient_info>();
            var vitalList = new List<Patient_vital>();
            var scheduleList = new List<Patient_schedule>();

            var query_info = new TableQuery<Patient_info>();
            var query_vital = new TableQuery<Patient_vital>();
            var query_schedule = new TableQuery<Patient_schedule>();
            TableContinuationToken continuationToken = null;

            do
            {
                var page = await table_info.ExecuteQuerySegmentedAsync(query_info, continuationToken);
                continuationToken = page.ContinuationToken;
                infoList.AddRange(page.Results);
            }
            while (continuationToken != null);
            continuationToken = null;

            do
            {
                var page = await table_vital.ExecuteQuerySegmentedAsync(query_vital, continuationToken);
                continuationToken = page.ContinuationToken;
                vitalList.AddRange(page.Results);
            }
            while (continuationToken != null);
            continuationToken = null;

            do
            {
                var page = await table_schedule.ExecuteQuerySegmentedAsync(query_schedule, continuationToken);
                continuationToken = page.ContinuationToken;
                scheduleList.AddRange(page.Results);
            }
            while (continuationToken != null);

            for (int i = 0; i < infoList.Count; i++)
            {
                if (infoList[i].PartitionKey == vitalList[i].PartitionKey && infoList[i].PartitionKey == scheduleList[i].PartitionKey)
                {
                    Patient newPatient = new Patient(infoList[i].PartitionKey, infoList[i].RowKey, infoList[i], vitalList[i], scheduleList[i]);
                    newPatientList.AddPatient(newPatient);
                }
            }

            GameManager.instance.UpdatePatientListData(newPatientList);

            //! For Test Log
            foreach (var i in GameManager.instance.patientList.patientList)
            {
                Debug.Log($"--------GetAllPatients---------");
                Debug.Log($"이름 : {i.patient_info.name}");
                Debug.Log($"주소 : {i.patient_info.address}");
                Debug.Log($"체온 : {i.patient_vital.body_temperature}");
                Debug.Log($"스케줄 : {i.patient_schedule.content}");
            }
        }

        //! For test 삭제요망
        public async void TestSelectPatient(string partitionKey)
        {
            TableOperation retrieveOperation;
            TableResult result;

            retrieveOperation = TableOperation.Retrieve<Patient_info>(partitionKey, partitionKey);
            result = await table_info.ExecuteAsync(retrieveOperation);
            Patient_info info = result.Result as Patient_info;

            retrieveOperation = TableOperation.Retrieve<Patient_vital>(partitionKey, partitionKey);
            result = await table_vital.ExecuteAsync(retrieveOperation);
            Patient_vital vital = result.Result as Patient_vital;

            retrieveOperation = TableOperation.Retrieve<Patient_schedule>(partitionKey, partitionKey);
            result = await table_schedule.ExecuteAsync(retrieveOperation);
            Patient_schedule schedule = result.Result as Patient_schedule;

            // Create Patient Instance
            Patient newPatient = new Patient(partitionKey, partitionKey, info, vital, schedule);
            GameManager.instance.UpdatePatientData(newPatient);

            Debug.Log($"--------SelectPatients---------");
            Debug.Log($"이름 : {GameManager.instance.patient.patient_info.name}");
            Debug.Log($"주소 : {GameManager.instance.patient.patient_info.address}");
            Debug.Log($"체온 : {GameManager.instance.patient.patient_vital.body_temperature}");
            Debug.Log($"스케줄 : {GameManager.instance.patient.patient_schedule.content}");
        }
    }
}





// DB에 저장할 데이터 구조
public class Patient
{
    public string pKey;
    public string rKey;

    public Patient_info patient_info = new Patient_info();
    public Patient_vital patient_vital = new Patient_vital();
    public Patient_schedule patient_schedule = new Patient_schedule();

    public Patient() { }
    public Patient(string PartitionKey, string RowKey, Patient_info info, Patient_vital vital, Patient_schedule schedule)
    {
        this.pKey = PartitionKey;
        this.rKey = RowKey;

        patient_info = info;
        patient_vital = vital;
        patient_schedule = schedule;
    }
}

public class Patient_info : TableEntity
{
    public string name { get; set; }
    public string birth { get; set; }
    public string age { get; set; }
    public string gender { get; set; }
    public string address { get; set; }
    public string disease_name { get; set; }
    public string ward { get; set; }

    public string room_number { get; set; }
    public string phone_number { get; set; }
    public string NOK { get; set; }
    public string NOK_phone_number { get; set; }

    public Patient_info()
    {
    }

    public Patient_info(string PartitionKey, string RowKey,
                    string name, string birth, string age, string gender, string address, string disease_name, string ward,
                    string room_number, string phone_number, string NOK, string NOK_phone_number)
    {
        this.PartitionKey = PartitionKey;
        this.RowKey = RowKey;

        this.name = name;
        this.birth = birth;
        this.age = age;
        this.gender = gender;
        this.address = address;
        this.disease_name = disease_name;
        this.ward = ward;
        this.room_number = room_number;
        this.phone_number = phone_number;
        this.NOK = NOK;
        this.NOK_phone_number = NOK_phone_number;
    }
}

public class Patient_vital : TableEntity
{
    public string blood_pressure_high { get; set; }
    public string blood_pressure_low { get; set; }
    public string body_temperature { get; set; }
    public string pulse_rate { get; set; }
    public string respiration_rate { get; set; }

    public Patient_vital() { }

    public Patient_vital(string PartitionKey, string RowKey,
                    string blood_pressure_high, string blood_pressure_low, string body_temperature, string pulse_rate, string respiration_rate)
    {
        this.PartitionKey = PartitionKey;
        this.RowKey = RowKey;

        this.blood_pressure_high = blood_pressure_high;
        this.blood_pressure_low = blood_pressure_low;
        this.body_temperature = body_temperature;
        this.pulse_rate = pulse_rate;
        this.respiration_rate = respiration_rate;
    }
}

public class Patient_schedule : TableEntity
{
    public string content { get; set; }
    public DateTime date { get; set; }

    public Patient_schedule() { }

    public Patient_schedule(string PartitionKey, string RowKey, string date, string content)
    {
        this.PartitionKey = PartitionKey;
        this.RowKey = RowKey;

        // 2021/07/06 19:52
        string format = "yyyy/MM/dd HH:mm";
        this.date = DateTime.ParseExact(date, format, System.Globalization.CultureInfo.InvariantCulture);

        this.content = content;
    }
}

public class PatientList
{
    public List<Patient> patientList = new List<Patient>();

    public void SetPatientList(List<Patient> newPatientList)
    {
        this.patientList = newPatientList;
    }

    public void AddPatient(Patient newPatient)
    {
        this.patientList.Add(newPatient);
    }
}