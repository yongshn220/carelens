﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MRTK.Tutorials.AzureCloudServices.Scripts.Managers;
using Random = UnityEngine.Random;



public class SetRandomValue : MonoBehaviour
{
    public enum STATE { blood_pressure = 0, body_temperature, pulse_rate, respiration_rate };
    public STATE state;

    void Start()
    {
        StartCoroutine(WriteRandomValue());
    }

    // 랜덤으로 설정
    IEnumerator WriteRandomValue()
    {
        Array values = Enum.GetValues(typeof(STATE));

        // 환자가 선택되지 않은 상태면 코드가 "-1"
        // 따라서 코드가 "-1"이 아닐 경우에만 데이터를 씀
        while (true)
        {
            yield return new WaitForSeconds(0.5f);

            if (GameManager.instance.patient == null) continue;

            // 랜덤으로 STATE(key) 뽑기
            STATE responses = (STATE)values.GetValue(Random.Range(0, values.Length));
            string key = Convert.ToString(responses);
            string value = "";

            // Debug.Log($"ㄴ바뀐 값 : {key}");

            int val1, val2;
            float val;
            // STATE(key) 따른 랜덤 value 값 뽑기
            switch (responses)
            {
                case STATE.blood_pressure:
                    val1 = Random.Range(120, 131);
                    val2 = Random.Range(80, 85);
                    value = val1 + "/" + val2;
                    break;
                case STATE.body_temperature:
                    val = Random.Range(35.9f, 37.6f);
                    value = string.Format("{0:0.0}", val);
                    // value = val.ToString();
                    break;
                case STATE.pulse_rate:
                    val1 = Random.Range(60, 101);
                    value = val1.ToString();
                    break;
                case STATE.respiration_rate:
                    val1 = Random.Range(12, 21);
                    value = val1.ToString();
                    break;
            }

            // gameObject.GetComponent<DataManager>().WriteData(key, value);

        }

    }

}
