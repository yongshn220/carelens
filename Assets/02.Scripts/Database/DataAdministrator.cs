using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.Storage.Table;
using MRTK.Tutorials.AzureCloudServices.Scripts.Domain;
using UnityEngine;
using UnityEngine.Events;
using TMPro;

// using Microsoft.Uii.Desktop.Core

namespace MRTK.Tutorials.AzureCloudServices.Scripts.Managers
{
    public class DataAdministrator : MonoBehaviour
    {
        // [SerializeField]
        private string connectionString = "DefaultEndpointsProtocol=https;AccountName=carelens-db;AccountKey=8QurGPH2shU1N8rVPxKlBK9MQyO9M9HlaGoLtOoKIMCwWgYazjFLwIlXN9JwA2nQRwsEqiFe0gnyhtaZ7jkqTw==;TableEndpoint=https://carelens-db.table.cosmos.azure.com:443/;";

        private CloudStorageAccount storageAccount;
        private CloudTableClient cloudTableClient;
        private CloudTable table_vital;

        private List<Patient_vital> vitalList;
        private bool isLoad = false;

        private void Awake()
        {
            storageAccount = CloudStorageAccount.Parse(connectionString);
            cloudTableClient = storageAccount.CreateCloudTableClient();
            table_vital = cloudTableClient.GetTableReference("patient_vital");

            StartCoroutine(CheckVitalData());
        }

        IEnumerator CheckVitalData()
        {
            while (true)
            {
                yield return new WaitForSeconds(1.0f);

                LoadAllPatientData();

                yield return new WaitUntil(() => isLoad);
                isLoad = false;

                CheckAbnormalData();

            }
        }

        // Load All Patient Data
        public async void LoadAllPatientData()
        {
            PatientList newPatientList = new PatientList();

            // CloudTable table = this.projectsTable;
            var newVitalList = new List<Patient_vital>();
            var query_vital = new TableQuery<Patient_vital>();
            TableContinuationToken continuationToken = null;

            do
            {
                var page = await table_vital.ExecuteQuerySegmentedAsync(query_vital, continuationToken);
                continuationToken = page.ContinuationToken;
                newVitalList.AddRange(page.Results);
            }
            while (continuationToken != null);
            continuationToken = null;

            vitalList = newVitalList;

            isLoad = true;
        }

        public void CheckAbnormalData()
        {

            foreach (Patient_vital p in vitalList)
            {
                if (Int32.Parse(p.blood_pressure_high) < 120 || Int32.Parse(p.blood_pressure_high) > 130 ||
                    Int32.Parse(p.blood_pressure_low) < 80 || Int32.Parse(p.blood_pressure_low) > 85 ||
                    float.Parse(p.body_temperature) < 35.9f || float.Parse(p.body_temperature) > 37.6f ||
                    Int32.Parse(p.pulse_rate) < 60 || Int32.Parse(p.pulse_rate) > 100 ||
                    Int32.Parse(p.respiration_rate) < 12 || Int32.Parse(p.respiration_rate) > 20)
                {
                    GameManager.instance.NotifyEmergency(p.PartitionKey);
                }
            }
        }

    }
}



