﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using MRTK.Tutorials.AzureCloudServices.Scripts.Managers;
using Random = UnityEngine.Random;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;


public class VitalTableCtrl : MonoBehaviour
{
    private string connectionString = "DefaultEndpointsProtocol=https;AccountName=carelens-db;AccountKey=8QurGPH2shU1N8rVPxKlBK9MQyO9M9HlaGoLtOoKIMCwWgYazjFLwIlXN9JwA2nQRwsEqiFe0gnyhtaZ7jkqTw==;TableEndpoint=https://carelens-db.table.cosmos.azure.com:443/;";

    private CloudStorageAccount storageAccount;
    private CloudTableClient cloudTableClient;
    private CloudTable table_info;
    private CloudTable table_vital;
    private CloudTable table_schedule;

    public float updateCycle = 1.0f;
    private bool isLoad = false;
    private bool isEmergency = false;

    List<Patient_vital> patientList = null;
    private string seletedPatientKey = "";

    string blood_pressure_high;
    string blood_pressure_low;
    string body_temperature;
    string pulse_rate;
    string respiration_rate;



    private void Awake()
    {
        storageAccount = CloudStorageAccount.Parse(connectionString);
        cloudTableClient = storageAccount.CreateCloudTableClient();
        table_vital = cloudTableClient.GetTableReference("patient_vital");
    }

    void Start()
    {
        StartCoroutine(WriteRandomValue());
    }

    // 랜덤으로 설정
    IEnumerator WriteRandomValue()
    {
        while (true)
        {
            yield return new WaitForSeconds(updateCycle);

            // Update Patient List
            LoadAllPatient();

            yield return new WaitUntil(() => isLoad);
            isLoad = false;

            foreach (Patient_vital p in patientList)
            {
                if (p.PartitionKey == seletedPatientKey) continue;

                // Extract values
                SetRandomValue();

                // Create Instance
                Patient_vital vitalData = new Patient_vital(p.PartitionKey, p.RowKey, blood_pressure_high, blood_pressure_low, body_temperature, pulse_rate, respiration_rate);

                // Update Database Vital Table
                UpdateVitalData(vitalData);
            }

            //! Test Log
            // Debug.Log($"seletedPatientKey : {seletedPatientKey}");
            // Debug.Log($"key : {vitalData.PartitionKey}");
            // Debug.Log($"blood_pressure_high : {vitalData.blood_pressure_high}");
            // Debug.Log($"blood_pressure_low : {vitalData.blood_pressure_low}");
            // Debug.Log($"body_temperature : {vitalData.body_temperature}");
            // Debug.Log($"pulse_rate : {vitalData.pulse_rate}");
            // Debug.Log($"respiration_rate : {vitalData.respiration_rate}");
            // Debug.Log("-----------------------------------------------");

        }
    }

    // Load All Patient Vital Data
    async void LoadAllPatient()
    {
        var newVitalList = new List<Patient_vital>();
        var query_vital = new TableQuery<Patient_vital>();
        TableContinuationToken continuationToken = null;

        do
        {
            var page = await table_vital.ExecuteQuerySegmentedAsync(query_vital, continuationToken);
            continuationToken = page.ContinuationToken;
            newVitalList.AddRange(page.Results);
        }
        while (continuationToken != null);
        continuationToken = null;

        patientList = newVitalList;

        isLoad = true;
    }

    // Select a Patient Randomly
    void SelectPatient()
    {
        // if (patientList.Count == 0) return;

        int idx = Random.Range(0, patientList.Count);
        seletedPatientKey = patientList[idx].PartitionKey;
    }

    void SetRandomValue()
    {
        blood_pressure_high = Random.Range(120, 131).ToString();
        blood_pressure_low = Random.Range(80, 86).ToString();
        float value = Random.Range(35.9f, 37.6f);
        body_temperature = string.Format("{0:0.0}", value);
        pulse_rate = Random.Range(60, 101).ToString();
        respiration_rate = Random.Range(12, 21).ToString();
    }

    void SetRandomValue_Emergency()
    {
        blood_pressure_high = Random.Range(132, 141).ToString();
        blood_pressure_low = Random.Range(86, 90).ToString();
        float value = Random.Range(37.7f, 39.0f);
        body_temperature = string.Format("{0:0.0}", value);
        pulse_rate = Random.Range(101, 121).ToString();
        respiration_rate = Random.Range(21, 30).ToString();
    }

    public void UpdateVitalData(Patient_vital vitalData)
    {
        TableOperation to;
        TableResult tr;

        to = TableOperation.InsertOrMerge(vitalData);
        tr = table_vital.ExecuteAsync(to).Result;
    }

    public void StartEmergency()
    {
        StartCoroutine(SetEmergency());
    }

    IEnumerator SetEmergency()
    {
        isEmergency = true;
        SelectPatient();
        Debug.Log($"SelectPatient : {seletedPatientKey}");

        while (isEmergency)
        {
            yield return new WaitForSeconds(1.0f);

            SetRandomValue_Emergency();

            if (isEmergency)
            {
                // Create Instance
                Patient_vital vitalData = new Patient_vital(seletedPatientKey, seletedPatientKey, blood_pressure_high, blood_pressure_low, body_temperature, pulse_rate, respiration_rate);
                // Update Database Vital Table
                UpdateVitalData(vitalData);
            }

        }
    }

    public void StopEmergency()
    {
        // StopCoroutine(SetEmergency());
        isEmergency = false;
        // Init Emergency Patient
        seletedPatientKey = "";
    }

}
