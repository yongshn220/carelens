﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.Utilities;
using Microsoft.MixedReality.Toolkit.UI;

public class SliderController : MonoBehaviour
{
    private float cellwidth;
    private float sliderValue;

    [SerializeField]
    private GridObjectCollection gridObjectCollection;
    private PinchSlider pinchSlider;

    // Start is called before the first frame update
    void Start()
    {
        pinchSlider = GetComponent<PinchSlider>();
    }

    public void UpdateSliderValue()
    {
        Debug.Log("Update slider");
        sliderValue = pinchSlider.SliderValue;
        cellwidth =  Mathf.Lerp(0.25f, 0.5f, sliderValue);
        gridObjectCollection.CellWidth = cellwidth;
        gridObjectCollection.UpdateCollection();
    }
}
