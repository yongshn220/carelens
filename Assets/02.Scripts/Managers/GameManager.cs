﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using UnityEngine.Events;
using UnityEngine.SceneManagement;
using MRTK.Tutorials.AzureCloudServices.Scripts.Managers;
using Photon.Pun;
using Photon.Realtime;

public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;

    // Information of the selected Patient.
    public Patient patient;

    // Information of the emergency Patient.
    public Patient emergencyPatient;

    // Information of all Patients.
    public PatientList patientList;

    // Validation of QR code Searching.
    public bool isQRSearchValid = true;

    // Selected patient's key
    private string selectedPatientKey;

    // Emergency patient's key
    public string emergencyPatientKey = "";

    // Ignored emergency patient's key
    public string ignorePatientKey = "";

    // Data Manager Script
    private DataManager dataManager;

    //Events
    public DefaultEvent ActivateUIEvent;
    public MultiView MultiViewEvent;
    public PatientDataEvent patientUpdateEvent;
    public PatientListDataEvent patientListUpdateEvent;

    //Scene
    public string perosnalCareSceneName;
    public string multiCareSceneName;
    public string QRSceneName;
    public string mainSceneName;

    //Menu
    public GameObject Menu;

    // Whether to load data
    public bool isSelectedPatientLoaded = false;
    public bool isEmergencyPatientLoaded = false;

    // Emergency Alerm UI
    public EmergencyAlarmManager emergencyAlarmManager;
    private int hasEmergencyPatient = 0;

    #region SINGLETON
    void Awake()
    {
        if (instance == null)   // 처음 실행했을 경우
        {
            instance = this;
            //DontDestroyOnLoad(this.gameObject);
        }
        else if (instance != this)  // n번째 호출됐을 경우
        {
            //Destroy(this.gameObject);
        }
    }

    #endregion

    #region INITIALIZE
    void Start()
    {
        this.patient = new Patient();
        this.patientList = new PatientList();
        this.dataManager = GetComponent<DataManager>();
        this.patientListUpdateEvent.AddListener(TestTest);
    }

    #endregion
    
    // QR On -> stop seraching QR Code; Start care.
    public void SaveQRCode(string key)
    {
        this.isQRSearchValid = false;
        StartCare(key);
    }


    // QR Off -> start searching QR Code; Stop care.
    public void DeleteQRCode()
    {
        this.isQRSearchValid = true;
        EndCare();
    }

    // Start Coroutine 
    public void StartCare(string key)
    {
        this.selectedPatientKey = key;
        StartCoroutine(LoadPatientData());

        //Send to Persoanl care scene 
        this.ActivateUIEvent.Invoke();
        this.MultiViewEvent.Invoke(true);
    }

    public void EndCare()
    {
        this.selectedPatientKey = null;
        StopCoroutine(LoadPatientData());

        this.MultiViewEvent.Invoke(false);
    }

    // Realtime Loading Patient Data 
    IEnumerator LoadPatientData()
    {
        this.dataManager.LoadPatientData(this.selectedPatientKey, this.selectedPatientKey);

        yield return new WaitUntil(() => this.isSelectedPatientLoaded);

        this.isSelectedPatientLoaded = false;

        while (this.patient != null)
        {
            /*
            Only for testing
            */
            //this.LoadPatientDataTest();

            this.dataManager.LoadPatientVitalData();

            yield return new WaitForSeconds(1.0f);
        }
    }

    // Realtime Loading Patient Data 
    IEnumerator LoadEmergencyPatientData()
    {
        this.dataManager.LoadPatientData(this.emergencyPatientKey, this.emergencyPatientKey);

        yield return new WaitUntil(() => this.isEmergencyPatientLoaded);

        this.isEmergencyPatientLoaded = false;

        // UI on
        emergencyAlarmManager.OnCanvas();

        yield return new WaitForSeconds(1.5f);

        hasEmergencyPatient--;

        // If there is no emergency patient
        if (hasEmergencyPatient == 0)
        {
            // UI off
            emergencyAlarmManager.OffCanvas();
        }
    }

    public void LoadPatientListData()
    {
        this.dataManager.LoadAllPatientData();
    }

    // Update information of the selected patient.
    public void UpdatePatientData(Patient newPatient)
    {
        this.patient = newPatient;
        this.patientUpdateEvent.Invoke(this.patient);

        this.isSelectedPatientLoaded = true;
    }

    // Update information of the emergency patient.
    public void UpdateEmergencyPatientData(Patient newPatient)
    {
        this.emergencyPatient = newPatient;

        this.isEmergencyPatientLoaded = true;
    }

    // Update information of all patients.
    public void UpdatePatientListData(PatientList newPatientList)
    {
        Debug.Log("Data Updated");
        this.patientList = newPatientList;
        this.patientListUpdateEvent.Invoke(this.patientList);
    }



    #region PHOTON_EMERGENCY_CALL

    public void NotifyEmergency(string newEmergencyPatientKey)
    {
        Debug.Log("~~~~~~~~~EMERGENCY~~~~~~~~~");
        Debug.Log($"newEmergencyPatientKey : {newEmergencyPatientKey}");

        GetComponent<PhotonView>().RPC("AlertEmergency", RpcTarget.AllViaServer, newEmergencyPatientKey);
    }

    [PunRPC]
    void AlertEmergency(string newPkey)
    {
        if (ignorePatientKey == newPkey) return;

        hasEmergencyPatient++;
        emergencyPatientKey = newPkey;

        StartCoroutine(LoadEmergencyPatientData());
    }
    #endregion






    #region TEST_FUNCTIONS
    void TestTest(PatientList newPatientList)
    {
        Debug.Log("Event Connected");
    }

    public void LoadPatientDataTest()
    {
        Patient testPatient = CreatePatient("Person_A");
        this.UpdatePatientData(testPatient);
    }

    public void LoadPatientListDataTest()
    {
        PatientList testPatientList = CreatePatientList();

        this.UpdatePatientListData(testPatientList);
    }


    Patient CreatePatient(string name)
    {
        Patient testPatient = new Patient();

        int rNum = UnityEngine.Random.Range(10, 60);
        string rNumStr = rNum.ToString();
        testPatient.patient_vital.blood_pressure_high = rNumStr + 1;
        testPatient.patient_vital.pulse_rate = rNumStr + 5;
        testPatient.patient_vital.respiration_rate = rNumStr + 2;
        testPatient.patient_vital.body_temperature = rNumStr + 3;

        testPatient.patient_info.name = name;
        testPatient.patient_info.age = "13";
        testPatient.patient_info.gender = "male";
        testPatient.patient_info.PartitionKey = "0001";
        testPatient.patient_info.phone_number = "01012341234";
        testPatient.patient_info.ward = "ward_A";
        testPatient.patient_info.NOK = "NOK_A";
        testPatient.patient_info.NOK_phone_number = "0109876532";
        testPatient.patient_info.room_number = "101";
        testPatient.patient_info.birth = "19990306";
        testPatient.patient_info.disease_name = "Cancer";
        return testPatient;
    }

    PatientList CreatePatientList()
    {
        PatientList testPatientList = new PatientList();

        testPatientList.AddPatient(this.CreatePatient("사람1"));
        testPatientList.AddPatient(this.CreatePatient("사람2"));
        testPatientList.AddPatient(this.CreatePatient("사람3"));
        testPatientList.AddPatient(this.CreatePatient("사람4"));
        testPatientList.AddPatient(this.CreatePatient("사람5"));
        testPatientList.AddPatient(this.CreatePatient("사람6"));
        testPatientList.AddPatient(this.CreatePatient("사람7"));
        testPatientList.AddPatient(this.CreatePatient("사람8"));
        testPatientList.AddPatient(this.CreatePatient("사람9"));
        testPatientList.AddPatient(this.CreatePatient("사람10"));
        testPatientList.AddPatient(this.CreatePatient("사람11"));
        testPatientList.AddPatient(this.CreatePatient("사람12"));

        return testPatientList;
    }

    #endregion

    #region SCENE
    public void EnterPersonalCareScene()
    {
        this.isQRSearchValid = true;
    }

    public void EnterMultiCareScene()
    {
        this.isQRSearchValid = false;
    }

    void SceneSetting()
    {

    }

    [ContextMenu("LoadPersonalCareScene")]
    public void LoadPersonalCareScene()
    {
        this.MenuOff();
        SceneManager.LoadSceneAsync(this.QRSceneName, LoadSceneMode.Additive);
        SceneManager.LoadSceneAsync(this.perosnalCareSceneName, LoadSceneMode.Additive);
    }

    [ContextMenu("LoadMultiCareScene")]
    public void LoadMultiCareScene()
    {
        this.MenuOff();
        SceneManager.LoadSceneAsync(this.multiCareSceneName, LoadSceneMode.Additive);
        SceneManager.LoadSceneAsync(this.perosnalCareSceneName, LoadSceneMode.Additive);
    }

    public void LoadMainScene()
    {
        SceneManager.LoadSceneAsync(this.mainSceneName);
    }

    void MenuOn()
    {
        this.Menu.SetActive(true);
    }

    void MenuOff()
    {
        this.Menu.SetActive(false);
    }

    public void MoveToOtherScene(GameObject obj, int sceneNum)
    {
        Scene scene = SceneManager.GetSceneByBuildIndex(sceneNum);
        SceneManager.MoveGameObjectToScene(obj, scene);
    }

    #endregion
}


#region CUSTOM_EVENT
[System.Serializable]
public class DefaultEvent : UnityEvent
{
}

[System.Serializable]
public class MultiView : UnityEvent<bool>
{
}

[System.Serializable]
public class PatientDataEvent : UnityEvent<Patient>
{
}

[System.Serializable]
public class PatientListDataEvent : UnityEvent<PatientList>
{
}

#endregion

#region PREV

/*
public class GameManager : MonoBehaviour
{
    public static GameManager instance = null;
    public Patient patient = null;
    public PatientList patientList = new PatientList();

    void Awake()
    {
        if (instance == null)   // 처음 실행했을 경우
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else if (instance != this)  // n번째 호출됐을 경우
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {

    }

    void Update()
    {

    }

    // Initialize
    public void InitPatient()
    {
        patient = null;
    }

    public void InitPatientList()
    {
        patientList = null;
    }

    // Set to Selected Patients
    public void SetPatient(string PartitionKey, string RowKey, Patient_info info, Patient_vital vital, Patient_schedule schedule)
    {
        patient.pKey = PartitionKey;
        patient.rKey = RowKey;

        patient.patient_info = info;
        patient.patient_vital = vital;
        patient.patient_schedule = schedule;
    }

    // Update Patient Vital Data
    public void UpdatePatient(Patient_vital vital)
    {
        patient.patient_vital = vital;
    }

    // Update Patient Schedule Data
    public void UpdatePatient(Patient_schedule schedule)
    {
        patient.patient_schedule = schedule;
    }

    public void AddToPatientList(Patient newPatient)
    {
        patientList.AddPatient(newPatient);
    }
}
*/

#endregion