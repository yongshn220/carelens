﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Microsoft.MixedReality.Toolkit.Input;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;
using Microsoft.MixedReality.Toolkit.UI;

public class ButtonManager : MonoBehaviour
{
    public TMP_Text myPatientName;
    public TMP_Text myPatientNum;
    public GameObject pinchSlider;

    private Interactable interactable;
    private Follow follow;
    private EyeTrackingTarget eyeTrackingTarget;

    void Start()
    {
        eyeTrackingTarget = GetComponent<EyeTrackingTarget>();
        follow = GameObject.Find("PatientListManager").GetComponent<Follow>();
        interactable = GetComponent<Interactable>();
    }

    public void OnCubeClicked()
    {
        Debug.Log(myPatientName.text);
        GameManager.instance.StartCare(myPatientNum.text);
    }

    public void RollBackButtonClicked()
    {
        GameManager.instance.LoadMainScene();
    }

    public void FollowToggle()
    {
        if (interactable.IsToggled == true)
        {
            follow.UpdateLinkedTransform = false;
        }
        else 
        {
            follow.UpdateLinkedTransform = true;
        }
    }

    public void SliderToggle()
    {
        if (interactable.IsToggled == true)
        {
            pinchSlider.SetActive(true);
        }
        else 
        {
            pinchSlider.SetActive(false);
        }
    }
}