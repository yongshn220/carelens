﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using Microsoft.MixedReality.Toolkit.Utilities;
using Microsoft.MixedReality.Toolkit.Utilities.Solvers;

public class PatientListManager : MonoBehaviour
{
    // 필요 컴포넌트
    private Follow follow;
    private Transform patientListManager;
    public GameObject patientPrefab;                    // 환자정보 저장할 프리팹
    public List<GameObject> patientPrefabList;          // DB의 데이터만큼 프리팹 생성해 저장할 리스트
    private GridObjectCollection gridObjectCollection;   // 타일 모양 정렬할 컴포넌트


    // Start is called before the first frame update
    void Start()
    {
        follow = GetComponent<Follow>();
        gridObjectCollection = GetComponent<GridObjectCollection>();
        GameManager.instance.patientListUpdateEvent.AddListener(PatientDataUpdate);
        GameManager.instance.LoadPatientListData();

        //ADDED
        this.CollectionSetting();
        GameManager.instance.MultiViewEvent.AddListener(OnMultiViewChange);
    }


    // 이벤트 통해 데이터 개수만큼 타일 생성 및 각 프리팹에 정보입력
    public void PatientDataUpdate(PatientList patientList)
    {
        // 메모리에 리스트 할당
        patientPrefabList = new List<GameObject>();
        patientListManager = GameObject.Find("PatientListManager").GetComponent<Transform>();
        foreach (var patient in patientList.patientList)
        {
            GameObject patientCube = Instantiate(patientPrefab,
                                                 Vector3.zero,
                                                 Quaternion.identity);

            TMP_Text patientName = patientCube.transform.GetChild(0).GetChild(0).GetChild(0).
            gameObject.GetComponent<TMP_Text>();

            TMP_Text patientNum = patientCube.transform.GetChild(0).GetChild(0).GetChild(1).
            gameObject.GetComponent<TMP_Text>();

            patientName.text = patient.patient_info.name;
            patientNum.text = patient.patient_info.PartitionKey;

            patientPrefabList.Add(patientCube);
            GameManager.instance.MoveToOtherScene(patientCube, 1);

            patientCube.transform.parent = patientListManager;
        }
        gridObjectCollection.UpdateCollection();
    }


#region ADDED
    public MultiViewType type = MultiViewType.Default;
    public GameObject AdjustMenu;

    public enum MultiViewType
    {
        Default,
        Hide,
    }

    private CustomObjectCollection defaultCollection;
    private CustomObjectCollection hideCollection;
    private CustomObjectCollection currentCollection;

    void CollectionSetting()
    {
        this.defaultCollection = new CustomObjectCollection(0.375f, 1.0f);
        this.hideCollection = new CustomObjectCollection(1.2f, 4.0f);
        this.currentCollection = new CustomObjectCollection(0.375f, 1.0f);
    }

    public void OnMultiViewChange(bool state)
    {   
        if(this.type == MultiViewType.Default && state)
        {
            this.type = MultiViewType.Hide;
            this.AdjustMenu.SetActive(false);
            StartCoroutine(MultiViewUpdate(hideCollection));
        }
        else if(this.type == MultiViewType.Hide && !state)
        {
            this.type = MultiViewType.Default;
            this.AdjustMenu.SetActive(true);
            StartCoroutine(MultiViewUpdate(defaultCollection));
        }
    }

    IEnumerator MultiViewUpdate(CustomObjectCollection aimColl)
    {
        float curtime = 0;
        float loopTime = 0.025f;
        while(curtime <= 1.0f)
        {   
            this.currentCollection.UpdateCollection(aimColl,curtime);
            this.GridObjectUpdateByCustomObjectCollection();
            curtime += loopTime;
            yield return new WaitForSeconds(loopTime);
        }
    }

    void GridObjectUpdateByCustomObjectCollection()
    {
        this.gridObjectCollection.CellWidth = this.currentCollection.width;
        this.gridObjectCollection.Radius = this.currentCollection.radius;
        this.gridObjectCollection.UpdateCollection();
    }
#endregion
}


public class CustomObjectCollection
{
    public float width = 0;
    public float radius = 0;

    public CustomObjectCollection(float w, float r)
    {
        this.width = w;
        this.radius = r;
    }

    public void UpdateCollection(CustomObjectCollection aimColl, float percent)
    {
        if(percent > 1) percent = 1;

        this.width = Mathf.Lerp(this.width, aimColl.width, percent);
        this.radius = Mathf.Lerp(this.radius, aimColl.radius, percent);
    }
}