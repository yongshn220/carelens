﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Microsoft.MixedReality.Toolkit.UI;

public class PersonalCareManager : MonoBehaviour
{
    public GameObject pageList;
    public GameObject menuList;

    void Start()
    {
        GameManager.instance.ActivateUIEvent.AddListener(StartCare);
        DefaultSetting();
    }

    
    void DefaultSetting()
    {
        pageList.GetComponent<PageListControl>().DefaultSetting();
        menuList.GetComponent<MenuListControl>().DefaultSetting();
    }

    [ContextMenu("StartCare")]
    public void StartCare()
    {
        menuList.GetComponent<MenuListControl>().ActivatePageMenu(true);
        pageList.GetComponent<PageListControl>().ActivatePages(true);
    }

    [ContextMenu("EndCare")]
    public void EndCare()
    {
        this.DefaultSetting();
        GameManager.instance.DeleteQRCode();
    }

    public void OnQRSearchButtonClick()
    {
        this.EndCare();
    }

    public void OnExitButtonClick()
    {
        GameManager.instance.LoadMainScene();
    }
}
