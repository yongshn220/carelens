﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class EmergencyAlarmManager : MonoBehaviour
{
    public GameObject panel;

    public TMP_Text patientName;
    public TMP_Text code;
    public TMP_Text blood_pressure_high;
    public TMP_Text blood_pressure_low;
    public TMP_Text body_temperature;
    public TMP_Text pulse_rate;
    public TMP_Text respiration_rate;

    private Patient patient;

    public void OnCanvas()
    {
        patient = GameManager.instance.emergencyPatient;

        this.UpdateCanvasData();

        panel.gameObject.SetActive(true);
    }

    public void UpdateCanvasData()
    {
        patientName.text = patient.patient_info.name;
        code.text = patient.pKey;
        blood_pressure_high.text = patient.patient_vital.blood_pressure_high;
        blood_pressure_low.text = patient.patient_vital.blood_pressure_low;
        body_temperature.text = patient.patient_vital.body_temperature;
        pulse_rate.text = patient.patient_vital.pulse_rate;
        respiration_rate.text = patient.patient_vital.respiration_rate;
    }

    public void OffCanvas()
    {
        panel.gameObject.SetActive(false);
    }

    public void OnExitButton()
    {
        OffCanvas();
        GameManager.instance.ignorePatientKey = GameManager.instance.emergencyPatientKey;

        StartCoroutine(InitIgnorePatientKey());
    }

    // Init ignorePatientKey After 10 Seconds
    public IEnumerator InitIgnorePatientKey()
    {
        yield return new WaitForSeconds(10.0f);
        GameManager.instance.ignorePatientKey = "";
    }
}
