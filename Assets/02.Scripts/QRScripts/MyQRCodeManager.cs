﻿using QRTracking;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class MyQRCodeManager : MonoBehaviour
{
    public QRCodesManager qRCodesManager;
    public TMP_Text statusText;
    public bool isTracking = false;

    //Test
    void Start()
    {
        Debug.Log("QR code detected");
        //GetQRCodeTest();
    }

    public void ToggleScan()
    {
        if(isTracking)
        {
            StopScan();
        }
        else 
        {
            StartScan();
        }
    }

    [ContextMenu("StartScan")]
    public void StartScan()
    {
        qRCodesManager.StartQRTracking();
        isTracking = !isTracking;
        statusText.text = "Started QRCode Tracking";
    }

    public void StopScan()
    {
        qRCodesManager.StopQRTracking();
        isTracking = !isTracking;
        statusText.text = "Stopped QRCode Tracking";
    }

    
    /*
    This is Testing Function
    */
    [ContextMenu("GetQRCodeTest")]
    public void GetQRCodeTest()
    {
        GameManager.instance.SaveQRCode("0001");
    }
}
