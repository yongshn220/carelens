﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using MRTK.Tutorials.AzureCloudServices.Scripts.Managers;

#if WINDOWS_UWP

using Windows.Perception.Spatial;

#endif
namespace QRTracking
{
    [RequireComponent(typeof(QRTracking.SpatialGraphCoordinateSystem))]
    public class QRCode : MonoBehaviour
    {
        public Microsoft.MixedReality.QR.QRCode qrCode;
        private GameObject qrCodeCube;

        public float PhysicalSize { get; private set; }
        public string CodeText { get; private set; }

        private TextMesh QRID;
        private TextMesh QRNodeID;
        private TextMesh QRText;
        private TextMesh QRVersion;
        private TextMesh QRTimeStamp;
        private TextMesh QRSize;
        private GameObject QRInfo;
        private bool validURI = false;
        private bool launch = false;
        private System.Uri uriResult;
        private long lastTimeStamp = 0;

        private bool isStopped;
        private bool isStopped2;
        private float nextTime = 0;
        private int currentTime = 0;
        private int lastTime = 0;

        private TMP_Text latestQRData;
        private TMP_Text lastTimeStampInfo;
        private TMP_Text lastTimeStampInfo1;
        private TMP_Text lastTimeStampInfo2;
        private TMP_Text lastTimeStampInfo3;
        //private DataManager dataManager;

        // Use this for initialization
        void Start()
        {
            PhysicalSize = 0.1f;
            CodeText = "Dummy";
            if (qrCode == null)
            {
                throw new System.Exception("QR Code Empty");
            }

            PhysicalSize = qrCode.PhysicalSideLength;
            CodeText = qrCode.Data;

            qrCodeCube = gameObject.transform.Find("Cube").gameObject;
            QRInfo = gameObject.transform.Find("QRInfo").gameObject;
            QRID = QRInfo.transform.Find("QRID").gameObject.GetComponent<TextMesh>();
            QRNodeID = QRInfo.transform.Find("QRNodeID").gameObject.GetComponent<TextMesh>();
            QRText = QRInfo.transform.Find("QRText").gameObject.GetComponent<TextMesh>();
            QRVersion = QRInfo.transform.Find("QRVersion").gameObject.GetComponent<TextMesh>();
            QRTimeStamp = QRInfo.transform.Find("QRTimeStamp").gameObject.GetComponent<TextMesh>();
            QRSize = QRInfo.transform.Find("QRSize").gameObject.GetComponent<TextMesh>();
            latestQRData = GameObject.Find("LatestQRData").GetComponent<TMP_Text>();

            lastTimeStampInfo = GameObject.Find("TimeStampTest").GetComponent<TMP_Text>();
            lastTimeStampInfo1 = GameObject.Find("TimeStampTest (1)").GetComponent<TMP_Text>();
            lastTimeStampInfo2 = GameObject.Find("TimeStampTest (2)").GetComponent<TMP_Text>();
            lastTimeStampInfo3 = GameObject.Find("TimeStampTest (3)").GetComponent<TMP_Text>();
            //dataManager = GameObject.Find("GameManager").GetComponent<DataManager>();

            QRID.text = "Id:" + qrCode.Id.ToString();
            QRNodeID.text = "NodeId:" + qrCode.SpatialGraphNodeId.ToString();
            QRText.text = CodeText;

            if (System.Uri.TryCreate(CodeText, System.UriKind.Absolute,out uriResult))
            {
                validURI = true;
                QRText.color = Color.blue;
            }

            QRVersion.text = "Ver: " + qrCode.Version;
            QRSize.text = "Size:" + qrCode.PhysicalSideLength.ToString("F04") + "m";
            QRTimeStamp.text = "Time:" + qrCode.LastDetectedTime.ToString("MM/dd/yyyy HH:mm:ss.fff");
            QRTimeStamp.color = Color.yellow;
            Debug.Log("Id= " + qrCode.Id + "NodeId= " + qrCode.SpatialGraphNodeId + " PhysicalSize = " + PhysicalSize + " TimeStamp = " + qrCode.SystemRelativeLastDetectedTime.Ticks + " QRVersion = " + qrCode.Version + " QRData = " + CodeText);
        }

        void UpdatePropertiesDisplay()
        {
            // Update properties that change
            if (qrCode != null && lastTimeStamp != qrCode.SystemRelativeLastDetectedTime.Ticks)
            {
                if (this.transform.GetChild(0).gameObject.activeSelf == false)
                {
                    this.transform.GetChild(0).gameObject.SetActive(true);
                    this.transform.GetChild(1).gameObject.SetActive(true);
                }

                QRSize.text = "Size:" + qrCode.PhysicalSideLength.ToString("F04") + "m";

                QRTimeStamp.text = "Time:" + qrCode.LastDetectedTime.ToString("MM/dd/yyyy HH:mm:ss.fff");
                QRTimeStamp.color = QRTimeStamp.color==Color.yellow? Color.white: Color.yellow;
                PhysicalSize = qrCode.PhysicalSideLength;
                Debug.Log("Id= " + qrCode.Id + "NodeId= " + qrCode.SpatialGraphNodeId + " PhysicalSize = " + PhysicalSize + " TimeStamp = " + qrCode.SystemRelativeLastDetectedTime.Ticks + " Time = " + qrCode.LastDetectedTime.ToString("MM/dd/yyyy HH:mm:ss.fff"));

                qrCodeCube.transform.localPosition = new Vector3(PhysicalSize / 2.0f, PhysicalSize / 2.0f, 0.0f);
                qrCodeCube.transform.localScale = new Vector3(PhysicalSize, PhysicalSize, 0.005f);
                lastTimeStamp = qrCode.SystemRelativeLastDetectedTime.Ticks;

                latestQRData.text = qrCode.Data;

                // if (GameManager.instance.patient.name == null)
                // {
                //     lastTimeStampInfo.text = "null";
                // }
                // else
                // {
                //     lastTimeStampInfo.text = GameManager.instance.patient.name;
                // }

                QRInfo.transform.localScale = new Vector3(PhysicalSize/0.2f, PhysicalSize / 0.2f, PhysicalSize / 0.2f);
            }
            currentTime = qrCode.SystemRelativeLastDetectedTime.Seconds;
            TimeCheck(currentTime);
        }

        /* public void StopTimeIncrease()
        {
            isStopped2 = true;
            lastTimeStampInfo1.text = $"isStopped2 = {isStopped2}";
        }

        public void StartTimeIncrease()
        {
            isStopped2 = false;
            lastTimeStampInfo1.text = $"isStopped2 = {isStopped2}";
        } */

        private void TimeCheck(int _currentTime)
        {
            if (Time.time > nextTime)
            {
                nextTime = Time.time + 1f;
                if (_currentTime == lastTime)
                {
                    isStopped = true;
                    lastTimeStampInfo.text = $"isStopped = {isStopped}";
                    StartCoroutine(QRCodeManagementCoroutine());
                }
                else 
                {
                    isStopped = false;
                    lastTimeStampInfo.text = $"isStopped = {isStopped}";
                    
                    /* if (GameManager.instance.isQRSearchValid == true)
                    {
                        GameManager.instance.SaveQRCode(qrCode.Data);
                    } */
                }
                lastTime = _currentTime;
            }
        }

        IEnumerator QRCodeManagementCoroutine()
        {
            yield return new WaitForSeconds(4.0f);
            if (isStopped == true)
            {
                lastTimeStampInfo1.text = "Deactivate";
                this.transform.GetChild(0).gameObject.SetActive(false);
                this.transform.GetChild(1).gameObject.SetActive(false);
            }
            else
            {
                lastTimeStampInfo1.text = "Reactivate";
            }
        }

        // Update is called once per frame
        void Update()
        {
            UpdatePropertiesDisplay();
            if (launch)
            {
                launch = false;
                LaunchUri();
            }
        }

        void LaunchUri()
        {
#if WINDOWS_UWP
            // Launch the URI
            UnityEngine.WSA.Launcher.LaunchUri(uriResult.ToString(), true);
#endif
        }

        public void OnInputClicked()
        {
            if (validURI)
            {
                launch = true;
            }
// eventData.Use(); // Mark the event as used, so it doesn't fall through to other handlers.
        }
    }
}