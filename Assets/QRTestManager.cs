﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class QRTestManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Invoke("GetQRCodeTest", 2.0f);
    }

    [ContextMenu("GetQRCodeTest")]
    public void GetQRCodeTest()
    {
        GameManager.instance.SaveQRCode("0001");
    }
}
